/*
 * tools.h
 * Copyright (C) 2018 streen <streen@gentoobox>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef TOOLS_H
#define TOOLS_H

#include <iostream>
#include <vector>
using namespace std;

namespace tools
{
    template <typename T>
    void vector_out(vector<T> v)
    {
        for (auto i : v)
        {
            cout << i << " ";
        }
        cout << endl;
    }

    template <typename T>
    void vector_min_max(vector<T> v)
    {
        T min=0, max=0;
        for (auto i : v)
        {
            if (i < min) min = i;
            else if (i > max) max = i;
        }
        cout << "min: " << min << ", max: " << max << endl;
    }
}

#endif /* !TOOLS_H */
