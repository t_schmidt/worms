#include <algorithm>
#include <chrono>
#include <memory>
#include <iostream>
#include <sstream>
#include <thread>
#include <vector>

#include "gameSdl.h"
#include "perlin1d.h"
#include "tools.h"

using namespace std;

/*
 * Logging
 */

namespace logging
{
    enum LOG_LEVEL
    {
        TRACE,
        DEBUG,
        INFO,
        WARN,
        ERROR,
        OFF
    };
    LOG_LEVEL CURRENT_LOG_LEVEL = OFF;
    void log(const string& msg, LOG_LEVEL l)
    {
        if (l >= CURRENT_LOG_LEVEL)
            cout << msg << endl;
    }
    void log(const string& msg)
    {
        log(msg, INFO);
    }
};

class Game;
class Worm;

/*
 * Teams
 */

typedef short teamId;

struct Team
{
    Team(Game* game, bool human) : m_bHuman(human)
    {
        id = idCounter++;
        color = Team::teamColors[id%4];
    }
    Team(Game* game) : Team(game,false) {}

    teamId id;
    bool m_bAlive = true;
    int m_iAliveWorms = 0;
    GameEngine::Color color;

    static int idCounter;
    static GameEngine::Color teamColors[];

    // AI
    bool m_bHuman = false;
};
typedef shared_ptr<Team> TeamPtr;
int Team::idCounter = 0;
GameEngine::Color Team::teamColors[] = {
    GameEngine::Color(255,0,0),
    GameEngine::Color(200,200,0),
    GameEngine::Color(0,255,0),
    GameEngine::Color(0,0,255),
};

/*
 * World Objects
 */

enum WOBJ_TYPE
{
    DEBRIS,
    DUMMY,
    MISSILE,
    WORM,
};

struct Collision
{
    Collision() {}
    Collision(bool collision)
            : collision(collision)
    {}
    bool collision = false;
    float refl_x = 0.0f;
    float refl_y = 0.0f;

    // values to perform the correction along the y-axis after left/right movement
    // those will only be set when the calculation is requested for the call to the
    // collision detection
    float ycorrX = 0.0f;
    float ycorrY = 0.0f;
    float ycorrD = 0.0f;
};

class WorldObject
{
    friend Game;
    friend Team;
public:
    WorldObject(Game* game, float x, float y)
            : m_game(game), x(x), y(y), vy(0.0f), vx(0.0f), m_bStable(false), m_bOnScreen(true)
    {
        id = WorldObject::_idCounter++;
    }
    virtual void draw() = 0;
    virtual void onObjectUpdate(float fElapsedTime) {}
    virtual void damage(int damage) {}
    virtual void toggleSelect() {m_bSelected = !m_bSelected;}
    bool isAlive() {return m_bAlive;}
    virtual WOBJ_TYPE type() = 0;
    bool isSelected() {return m_bSelected;}
    Collision collisionDetection(float,float,bool);
protected:
    void correctPosition(int width)
    {
        if (x - m_fRadius <= 0)
            x = m_fRadius + 1;
        else if (x + m_fRadius >= width)
            x = width - m_fRadius - 1;
    }
    Game* m_game;
    unsigned int id = 0;
    static unsigned int _idCounter;
    teamId team = -1;
    // size and position
    float x;
    float y;
    float vx;
    float vy;
    float m_fRadius = 15.0f;
    // health, probably only used by worms
    int m_iHealth = 100;
    bool m_bAlive = true;
    // movement: defaults are set for worms
    bool m_bStable;
    bool m_bOnScreen;
    bool m_bSelected = false;
    float m_fMoveThreshold = 0.2f;
    float m_fBounceFriction = 0.4f;
    // detonation
    bool m_bDetonated = false;
    int m_iDetonateRadius = 0;
    int m_iDetonateDmg = 20;
    // TTL
    float m_fTtl = 0.0f;
    bool m_bExpires = false;
};
typedef shared_ptr<WorldObject> WObj;
unsigned int WorldObject::_idCounter = 0;

class Dummy : public WorldObject
{
public:
    Dummy(Game* game, float x, float y)
        : WorldObject(game, x, y)
    {
        m_iDetonateRadius = 20;
    }
    virtual void draw();
    virtual WOBJ_TYPE type() {return DUMMY;}
};

class Missile : public WorldObject
{
public:
    Missile(Game* game, float x, float y, float _vx, float _vy)
        : WorldObject(game, x, y)
    {
        m_iDetonateRadius = 40;
        m_iDetonateDmg = 20;
        m_fRadius = 10;
        vx = _vx;
        vy = _vy;
    }
    virtual void draw();
    virtual WOBJ_TYPE type() {return MISSILE;}
};

class Debris : public WorldObject
{
public:
    Debris(Game* game, float x, float y, float _vx, float _vy)
        : WorldObject(game, x, y)
    {
        vx = _vx;
        vy = _vy;
        // the radius is deliberately smaller than the draw width of this class to prevent gaps between the
        // object and the terrain
        m_fRadius = 3.0f;
        // ttl
        m_fTtl = 5.0f + (float)(rand()%30)/10.0f;
        m_bExpires = true;
        m_fMoveThreshold = 0.01f;
        m_fBounceFriction = 0.7f;
    }
    virtual void draw();
    virtual WOBJ_TYPE type() {return DEBRIS;}
    int m_iDrawWidth = 5;
};

class Worm : public WorldObject
{
    friend Game;
    friend Team;
public:
    Worm(Game* game, float x, float y, teamId _team)
        : WorldObject(game, x, y)
    {
        team = _team;
        init();
    }
    virtual void draw();
    virtual void damage(int);
    virtual void onObjectUpdate(float);
    virtual WOBJ_TYPE type() {return WORM;}
    virtual void toggleSelect();
    void aimStart(bool);
    void aimStop();
    void init();
    void jump();
    void moveLeft();
    void moveRight();
    void moveStop();
    void weaponShoot();
    void weaponCharge(float);
private:
    int m_iWidth = m_fRadius * 1.414f;
    TeamPtr m_pTeam;
    // health
    static const int HEALTH_DRAW_WIDTH = 48;
    static const int HEALTH_PIX_PER_HEALTH = 100 / HEALTH_DRAW_WIDTH;
    // movement
    bool m_bLeft = true;
    bool m_bMoving = false;
    float m_fMoveDist = 0.0f;
    float m_fMovePerSecond = 80.0f;
    float m_fMoveThreshold = 1.0f;
    // weapon aiming
    bool m_bAiming = false;
    float m_bAimUp = true;
    float m_fAim = 0.0f;
    float m_fAimRadius = 40.0f;                                  // weapon pointer circle radius
    // weapon charging
    // TODO cleanup not needed members
    bool m_bCharging = false;
    float m_fCharge = 0.0f;
    float m_fChargeStartTime = 0.0f;
    static const int CHARGE_DRAW_WIDTH = 48;                       // in pixels, also used for the health bar
    static const constexpr float CHARGE_MAX_VALUE = 24.0f;      // equals max velocity from charge
    static const constexpr float CHARGE_MAX_TIME = 2.0f;        // in seconds
    static const constexpr float CHARGE_PER_SECOND =
            CHARGE_MAX_VALUE / CHARGE_MAX_TIME;                 // number of charges per second
    static const constexpr float CHARGE_PIX_PER_CHARGE =
            CHARGE_DRAW_WIDTH / CHARGE_MAX_VALUE;               // number of pixels to draw per charge
};

enum TERRAIN {
    T_NONE,
    T_SOLID,
};

/*
 * Game
 */

enum FSM_AI
{
    STATE_AI_AIM,
    STATE_AI_MOVE,
    STATE_AI_MOVING,
    STATE_AI_SELECTWORM,
    STATE_AI_SHOOT,
    STATE_AI_STABILIZE,
    STATE_AI_TARGET,
};
enum FSM_TURN
{
    STATE_END,
    STATE_MOVE,
    STATE_CHARGE,
    STATE_POST_GAME,
    STATE_STABILIZE,
    STATE_STABILIZE_JUMP
};

class Game : public GameEngine::Engine
{
    friend Debris;
    friend Dummy;
    friend Missile;
    friend Team;
    friend Worm;
    friend WorldObject;
public:
    Game()
    {
        // init ground level line
        srand(1234);
        Perlin1d p1d(m_iWidth,false);
        p1d.perlin_range(7, 2.6f, 6.0f);
        auto values = p1d.get_values();

        for (int i=0; i<m_iWidth; ++i)
        {
            vector<TERRAIN> m_iHeightVector;
            for (int j=0; j<m_iHeight; ++j)
                m_iHeightVector.push_back(T_NONE);
            m_vTerrain.push_back(m_iHeightVector);
        }
        for (int i=1; i<m_iWidth; ++i)
        {
            for (int j=values[i]+m_groundLevel; j>0; --j)
                m_vTerrain[i][j] = T_SOLID;
        }
        // add some teams
        srand(time(nullptr));
        for (int i=0; i<m_iTeams; ++i)
        {
            m_teams.push_back(TeamPtr(new Team(this,false)));
            //m_teams.push_back(TeamPtr(new Team(this,i==0)));
            m_iAliveTeams++;
        }
        for (auto team : m_teams)
        {
            for (int i=0; i<m_iWormsPerTeam; ++i)
            {
                int x = rand() % m_iWidth;
                int y = m_iHeight;
                for (; y>0; --y)
                    if (m_vTerrain[40][y] == T_SOLID)
                        break;
                createWorm(x, m_iHeight-10, team->id);
            }
        }

        // select the first Worm on the currently active team
        selectFirstWorm();
    }

    TeamPtr getTeam(teamId id)
    {
        for (auto t : m_teams)
            if (t->id == id)
                return t;
        return nullptr;
    }

    void addMissile(float x, float y, float vx, float vy)
    {
        WObj missile(new Missile(this, x, y, vx, vy));
        m_worldObjects.push_back(missile);
    }

    WObj getSelectedWObj()
    {
        auto obj = find_if(
                m_worldObjects.begin(),
                m_worldObjects.end(),
                [](WObj o){return o->isSelected();});
        return obj != m_worldObjects.end()
                ? *obj
                : nullptr;
    }

    // may return nullptr
    Worm* getSelectedWorm()
    {
        WObj obj = getSelectedWObj();
        return obj == nullptr
                ? nullptr
                : static_cast<Worm*>(obj.get());
    }

    void selectWormMaxHealth()
    {
        teamId team = m_teams[m_iActiveTeam]->id;
        int max = 0;
        Worm* worm = nullptr;
        forAllAliveWorms(
                [&worm,&max,team](Worm* w)
                {
                    if (w->m_pTeam->id==team && w->m_iHealth>max)
                    {
                        worm = w;
                        max = w->m_iHealth;
                    }
                });
        getSelectedWorm()->toggleSelect();
        worm->toggleSelect();
    }

private:
    virtual void onGameUpdate(float fElapsedTime);

    void createDummy(int x, int y)
    {
        WObj obj = WObj(new Dummy(this, x, y));
        obj->correctPosition(m_iWidth);
        m_worldObjects.push_back(obj);
    }

    void createWorm(int x, int y, teamId team)
    {
        WObj obj = WObj(new Worm(this, x, y, team));
        getTeam(team)->m_iAliveWorms += 1;
        m_worldObjects.push_back(obj);
        // fix position + set selection state
        obj->correctPosition(m_iWidth);
    }
    void createWorm(int x, int y)
    {
        createWorm(x, y, 0);
    }
    void wormDead(Worm* worm)
    {
        auto team = worm->m_pTeam;
        team->m_iAliveWorms--;
        if (team->m_iAliveWorms == 0)
        {
            team->m_bAlive = false;
            m_iAliveTeams--;
        }
    }

    void gameEnd()
    {
        cout << "game ended" << endl;
        auto team = find_if(
                m_teams.begin(),
                m_teams.end(),
                [](TeamPtr t){return t->m_bAlive;});
        if (team == m_teams.end())
            cout << "nobody has won" << endl;
        else
            cout << "team " << (*team)->id << " has won" << endl;
        m_eState = STATE_POST_GAME;
    }

    void gamePostGame() {};
    void gameTurn(float);
    void gameTurnMove(float);
    void gameTurnCharge(float);
    void gameTurnStabilize(float);
    void gameTurnStabilizeJump(float);

    void forAll(auto func)
    {
        for (WObj obj : m_worldObjects)
            func(obj);
    }
    void forAllWorms(auto func)
    {
        for (WObj obj : m_worldObjects)
            if (obj->type() == WORM)
            {
                Worm* worm = static_cast<Worm*>(obj.get());
                func(worm);
            }
    }
    void forAllWorms(auto pred, auto func)
    {
        for (WObj obj : m_worldObjects)
            if (obj->type() == WORM)
            {
                Worm* worm = static_cast<Worm*>(obj.get());
                if (pred(worm))
                    func(worm);
            }
    }
    void forAllAliveWorms(auto func)
    {
        for (WObj obj : m_worldObjects)
            if (obj->type() == WORM)
            {
                Worm* worm = static_cast<Worm*>(obj.get());
                if (worm->m_bAlive)
                    func(worm);
            }
    }
    void forAllTeamWorms(auto func, teamId team)
    {
        for (WObj obj : m_worldObjects)
            if (obj->type() == WORM && obj->team == team)
            {
                Worm* worm = static_cast<Worm*>(obj.get());
                func(worm);
            }
    }

    void move(float deltaX)
    {
        WObj w = getSelectedWObj();
        if (w == nullptr)
            return;
        if (w->m_bStable == false)
            return;
        w->m_bStable = false;
        w->vy -= 4.0f;
        w->vx += deltaX;
    }

    void moveLeft()
    {
        move(-2.0f);
    }

    void moveRight()
    {
        move(2.0f);
    }

    // will take m_iActiveTeam and select the first Worm from it
    void selectFirstWorm()
    {
        auto obj = find_if(
            m_worldObjects.begin(),
            m_worldObjects.end(),
            [this](WObj obj){return obj->type()==WORM && obj->team==m_iActiveTeam && obj->m_bAlive;});
        Worm* worm = static_cast<Worm*>((*obj).get());
        worm->toggleSelect();
    }

    void selectNextWorm()
    {
        // currently selected -> unselect
        auto selected = find_if(
                m_worldObjects.begin(),
                m_worldObjects.end(),
                [](WObj obj){return obj->m_bSelected;});
        Worm* worm = static_cast<Worm*>((*selected).get());
        if (worm->m_bCharging)
            return;
        worm->toggleSelect();
        teamId currentTeam = worm->team;
        selected++;

        // next worm in line
        auto next = find_if(
                selected,
                m_worldObjects.end(),
                [currentTeam](WObj obj){
                        return obj->type()==WORM
                            && obj->isAlive()==true
                            && obj->team==currentTeam;});

        // maybe search again from the beginning
        if (next == m_worldObjects.end())
            next = find_if(
                    m_worldObjects.begin(),
                    next,
                    [currentTeam](WObj obj){
                            return obj->type()==WORM
                            && obj->isAlive()==true
                            && obj->team==currentTeam;});

        (*next)->toggleSelect();
    }

    void selectNextTeam()
    {
        Worm* worm = getSelectedWorm();
        if (worm != nullptr)
            worm->toggleSelect();
        for (int j=0; j<m_teams.size(); ++j)
        {
            ++m_iActiveTeam;
            m_iActiveTeam %= m_iTeams;
            if (m_teams[m_iActiveTeam]->m_bAlive)
            {
                selectFirstWorm();
                break;
            }
        }
        if (m_teams[m_iActiveTeam]->m_bHuman == false)
        {
            m_pAiTarget = nullptr;
            m_eStateAI = STATE_AI_SELECTWORM;
        }
    }

    void wormMoveLeft()
    {
        Worm* w = getSelectedWorm();
        if (w != nullptr)
        {
            w->moveLeft();
        }
    }

    void wormMoveRight()
    {
        Worm* w = getSelectedWorm();
        if (w != nullptr)
        {
            w->moveRight();
        }
    }
    void wormStopMoving()
    {
        Worm* w = getSelectedWorm();
        if (w != nullptr)
        {
            w->moveStop();
        }
    }

    void wormJump()
    {
        Worm* w = getSelectedWorm();
        if (w != nullptr)
            w->jump();
    }

    void wormStartAim(bool aimUp)
    {
        Worm* w = getSelectedWorm();
        if (w != nullptr)
        {
            w->aimStart(aimUp);
        }
    }

    void wormStopAim()
    {
        Worm* w = getSelectedWorm();
        if (w != nullptr)
        {
            w->aimStop();
        }
    }

    int m_iHeight = 400;
    int m_iWidth = 400;
    int m_groundLevel = 200;
    vector<vector<TERRAIN>> m_vTerrain;

    // physics constants for all world objects
    float velocityThreshold = 0.1f;
    float gravity = -9.81f;

    vector<TeamPtr> m_teams;
    int m_iActiveTeam = 0;
    int m_iTeams = 2;
    int m_iWormsPerTeam = 2;
    int m_iAliveTeams = 0;          // will be incremented with each added team
    vector<WObj> m_worldObjects;

    float accumulateTime = 0.0f;
    float accumulateTimeInfo = 0.0f;
    float wantFPS = 1.0f/50.0f;
    float wantFPSInfo = 1.0f/5.0f;
    // stats to be updated on-screen less frequently than the usual drawing rate
    int m_iFps = 0.0f;
    int m_iPps = 0.0f;
    unsigned int m_iFpsCounter = 0;
    unsigned int m_iPpsCounter = 0;

    bool m_bStable = false;
    float m_fTurnTime = 0.0f;
    float m_fTurnTimeMax = 40.0f;

    // AI
    FSM_AI m_eStateAI = STATE_AI_SELECTWORM;
    Worm* m_pAiTarget = nullptr;
    float m_fAiTargetDistMin = 150.0f;
    float m_fAiTargetDistMax = 170.0f;
    float m_fAiCharge = 0.0f;
    float m_fAiAim = 0.0f;

    void aiDecision(float time);
    void aiCalcTarget();
    bool aiMoveIntoRange();

    // turn management
    FSM_TURN m_eState = STATE_MOVE;
    vector<SDL_Event> m_vEvents;
};

// collisions are checked only the direction of the object's movement
// this is limited to a half-circle area with the movement vector in the center
/*
 * @param newX: new object position, x component
 * @param newY: new object position, y component
 */
Collision WorldObject::collisionDetection(float newX, float newY, bool ycorr=false)
{
    float lenFact = m_fRadius / sqrtf(vx*vx + vy*vy);
    float xc = newX + vx * lenFact;
    float yc = newY + vy * lenFact;

    float te = acos((xc - newX) / m_fRadius);
    float pi = 3.14519f;
    float pihalf = 3.14519f / 2.0f;
    float step = 3.14159f / 8.0f;
    float respx =  0.0f;
    float respy =  0.0f;
    bool bCollision = false;
    // correct the parameter for the lower semi-circle
    if (yc < newY)
        te = pi + pi - te;

    // min dist to terrain (in movement direction)
    // will be used for the y adjustment after left/right movement
    float distMin = 600.0f; // TODO max of height and width
    float txMin = 0.0f;
    float tyMin = 0.0f;

    for (float p=te-pihalf; p<=te+pihalf; p+=step)
    {
        float px = newX + m_fRadius * cos(p);
        float py = newY + m_fRadius * sin(p);
        if (px>=0 && px<m_game->m_iWidth && py>=0 && py<m_game->m_iHeight)
        {
            if (m_game->m_vTerrain[(int)px][(int)py] == T_SOLID)
            {
                respx += newX - px ;
                respy += newY - py ;
                bCollision = true;
                if (ycorr)
                {
                    float ycorrStep = m_fRadius / 8.0f;
                    for (float d=0.0f; d<=m_fRadius; d+=ycorrStep)
                    {
                        float tx = x + d * cos(p);
                        float ty = y + d * sin(p);
                        if (m_game->m_vTerrain[tx][ty])
                        {
                            if (d < distMin)
                            {
                                distMin = d;
                                txMin = tx;
                                tyMin = ty;
                            }
                        }
                    }
                }
            }
        }
    }

    // length of the response vector, for normalization
    float respLen = sqrtf(respx*respx + respy*respy);
    float respsx = respx/respLen;
    float respsy = respy/respLen;

    // r = -2 * d * n + v
    float dotn = vx * respsx + vy * respsy;
    float rx = -2.0f * dotn * respsx + vx;
    float ry = -2.0f * dotn * respsy + vy;

    Collision collision(bCollision);
    if (bCollision)
    {
        collision.refl_x = rx;
        collision.refl_y = ry;
    }
    if (ycorr)
    {
        collision.ycorrX = txMin;
        collision.ycorrY = tyMin;
        collision.ycorrD = distMin;
    }

    return collision;
}

void Dummy::draw()
{
    m_game->drawCircle(x, y, m_fRadius, GameEngine::Color(50,50,50));
    m_game->drawCircle(x, y, m_iDetonateRadius, GameEngine::Color(0,255,0));
    float fLenFact = m_fRadius / sqrtf(vx*vx + vy*vy);
    m_game->drawLine(x,y,x+vx*fLenFact,y+vy*fLenFact, GameEngine::Color(255,0,0));
}

void Debris::draw()
{
    // color mapping for the debris, w.r.t ttl
    //8: 255,255,150
    //6: 255,242,39
    //4: 255,127,39
    //2: 237,28,36
    //0: 137,0,21

    int i_ttl = (int)m_fTtl;
    int i_dist = i_ttl - (i_ttl % 2);
    float rem = m_fTtl - (float)i_dist;
    float dist = rem / 2.0f;// / rem;

    int r = 255;
    int g = 255;
    int b = 150;
    if (m_fTtl <= 2.0f)
    {
        r = 100.0f * dist + 100;
        g = 28 * dist;
        b = 15 * dist + 21;
    }
    else if (m_fTtl <= 4.0f)
    {
        r = 18.0f * dist + 237;
        g = 99 * dist + 28;
        b = 3 * dist + 36;
    }
    else if (m_fTtl <= 6.0f)
    {
        g = 115 * dist + 127;
        b = 39;
    }
    else
    {
        g = 13 * dist + 242;
        b = 111 * dist + 39;
    }

    m_game->drawRectangle(x, y, m_iDrawWidth, m_iDrawWidth, GameEngine::Color(r,g,b));
}

void Missile::draw()
{
    m_game->drawCircle(x, y, m_fRadius, GameEngine::Color(150,150,50));
    m_game->drawCircle(x, y, m_iDetonateRadius, GameEngine::Color(200,200,0));
    float fLenFact = m_fRadius / sqrtf(vx*vx + vy*vy);
    m_game->drawLine(x,y,x+vx*fLenFact,y+vy*fLenFact, GameEngine::Color(255,0,0));
}

void Worm::aimStart(bool aimUp)
{
    m_bAimUp = aimUp;
    m_bAiming = true;
}
void Worm::aimStop()
{
    m_bAiming = false;
}
void Worm::damage(int damage)
{
    m_iHealth -= damage;
    if (m_iHealth <= 0)
        m_bAlive = false;
}
void Worm::draw()
{
    m_game->drawText(to_string(id), x-10, y+m_fRadius+10);
    if (m_bAlive == false)
    {
        m_game->drawRectangle(x, y, m_fRadius, m_fRadius, GameEngine::Color(70,70,70), true);
    }
    else
    {
        m_game->drawCircle(x,y,m_fRadius,GameEngine::Color(200,200,200));
        // worm
        m_game->drawRectangle(x-m_iWidth/2, y+m_iWidth/2, m_iWidth, m_iWidth, m_pTeam->color, m_bSelected);
        int lrRectX = m_bLeft ? x-m_iWidth/2 : x+m_iWidth/2-4;
        m_game->drawRectangle(lrRectX, y+m_iWidth/2, 4, m_iWidth, GameEngine::Color(100,100,220), true);
        // health
        m_game->drawRectangle(x+10, y+10, HEALTH_DRAW_WIDTH, 10, GameEngine::Color(40,180,40));
        m_game->drawRectangle(x+10, y+10, m_iHealth / HEALTH_PIX_PER_HEALTH, 10, GameEngine::Color(120,240,120), true);
        // weapon aim
        m_game->drawText(to_string(m_fAim), x, y+20, GameEngine::Color(240, 150, 90));
        float aimX = x + (m_bLeft?-1.0f:1.0f) * m_fAimRadius * sin(3.14-m_fAim);
        float aimY = y + m_fAimRadius * cos(3.14-m_fAim);
        m_game->drawRectangle(aimX, aimY, 5, 5, GameEngine::Color(50,240,0), true);
        // target to distance
        if (m_game->m_pAiTarget)
        {
            Worm* sel = m_game->getSelectedWorm();
            if (id==sel->id)
            {
                float targetDist = abs(m_game->m_pAiTarget->x - x);
                m_game->drawText(to_string(targetDist), x, y+30, GameEngine::Color(80,80,80));
            }
        }
        // weapon charge
        if (m_bCharging)
        {
            m_game->drawRectangle(x+10, y-20, CHARGE_DRAW_WIDTH, 10, GameEngine::Color(180,40,40));
            m_game->drawRectangle(x+10, y-20, CHARGE_PIX_PER_CHARGE * m_fCharge, 10, GameEngine::Color(240,120,120), true);
        }
        // maybe the AI is aiming at this worm
        if (m_game->m_pAiTarget && m_game->m_pAiTarget==this)
        {
            m_game->drawCircle(x, y, 20, GameEngine::Color(255,0,0));
            float halfrad = m_fRadius/2;
            m_game->drawLine(x, y-25, x, y+25, GameEngine::Color(255,0,0));
            m_game->drawLine(x-25, y, x+25, y, GameEngine::Color(255,0,0));
        }
    }
}
void Worm::moveLeft()
{
    m_bLeft = true;
    m_bMoving = true;
}
void Worm::moveRight()
{
    m_bLeft = false;
    m_bMoving = true;
}
void Worm::moveStop()
{
    m_bMoving = false;
    m_fMoveDist = 0.0f;
}
void Worm::init()
{
    m_pTeam = m_game->getTeam(team);
}
void Worm::jump()
{
    if (m_bLeft)
        vx -= 2.0f;
    else
        vx += 2.0f;
    vy += 5.0f;
    m_bStable = false;
}
void Worm::weaponCharge(float fElapsedTime)
{
    m_bCharging = true;
    m_fCharge += fElapsedTime * CHARGE_PER_SECOND;
}
void Worm::weaponShoot()
{
    // aim point, but the radius corresponds to the amount of weapon charge
    float ax = m_fCharge * sin(3.14159f-m_fAim);
    float ay = m_fCharge * cos(3.14159f-m_fAim);
    if (m_bLeft)
        ax *= -1.0f;

    // the missile
    m_game->addMissile(x, y, ax, ay);

    m_fCharge = 0.0f;
    m_bCharging = false;
}
void Worm::toggleSelect()
{
    m_bSelected = !m_bSelected;
}
void Worm::onObjectUpdate(float fElapsedTime)
{
    if (m_bMoving)
    {
        m_fMoveDist += m_fMovePerSecond * fElapsedTime;
        if (m_fMoveDist >= m_fMoveThreshold)
        {
            int deltaX = m_fMoveDist * (m_bLeft ? -1 : 1);
            x += deltaX;
            Collision collision = collisionDetection(x, y, true);
            if (collision.ycorrY > 0)
                y = m_fRadius + collision.ycorrY;
            m_bStable = false;
            m_fMoveDist = 0.0f;
        }
    }
    if (m_bAiming)
    {
        if (m_bAimUp)
        {
            m_fAim += 2 * fElapsedTime;
            if (m_fAim > 3.14159f)
                m_fAim = 3.14159f;
        }
        else
        {
            m_fAim -= 2 * fElapsedTime;
            if (m_fAim < 0.0f)
                m_fAim = 0.0f;
        }
    }
}

void Game::onGameUpdate(float fElapsedTime)
{
    // physics
    logging::log("Calculating physics", logging::TRACE);

    // will be updated in the following loop
    m_bStable = true;
    // will be updated in the following loop
    vector<WObj> newObjects;
    for (auto obj : m_worldObjects)
    {
        if (obj->m_bStable == false)
        {
            if (obj->type() == WORM || obj->type() == MISSILE)
                m_bStable = false;
            obj->vy += gravity * fElapsedTime;
            // TODO replace all newX by xNew or xn or something, same for y
            float newX = obj->x + obj->vx * fElapsedTime * 20.0f;
            float newY = obj->y + obj->vy * fElapsedTime * 20.0f;

            // collision detection
            Collision collision = obj->collisionDetection(newX, newY);

            // detonate!
            // this will skip the rest of this loop
            if (collision.collision && obj->m_iDetonateRadius > 0)
            {
                int tilesExploded = 0;
                int detr = obj->m_iDetonateRadius;
                for (int detx=newX-detr; detx<=newX+detr; ++detx)
                {
                    for (int dety=newY-detr; dety<=newY+detr; ++dety)
                    {
                        int dx = abs(newX - detx);
                        int dy = abs(newY - dety);
                        int dxy = sqrt(dx*dx + dy*dy);
                        if (dxy < detr)
                        {
                            int _x = detx < 0 ? 0 : (detx >= m_iWidth ? m_iWidth-1 : detx);
                            int _y = dety < 0 ? 0 : (dety >= m_iHeight ? m_iHeight-1 : dety);
                            if (m_vTerrain[_x][_y] == T_SOLID)
                            {
                                ++tilesExploded;
                                m_vTerrain[_x][_y] = T_NONE;
                            }
                        }
                    }
                }
                obj->m_bDetonated = true;
                int numDebris = tilesExploded / 40;
                for (int i=0; i<numDebris; ++i)
                {
                    WObj debris(new Debris(this, newX, newY, rand()%14-7, rand()%14-7));
                    newObjects.push_back(debris);
                }
                // check if any object is affected by the detonation
                for (auto otherObj : m_worldObjects)
                {
                    if (otherObj->m_bAlive == false)
                        continue;
                    int distX = abs(otherObj->x - obj->x);
                    if (distX <= obj->m_iDetonateRadius)
                    {
                        int distY = abs(otherObj->y - obj->y);
                        int distObj = sqrt(distX*distX - distY*distY);
                        // the object might be floating in the air now
                        if (distObj <= obj->m_iDetonateRadius)
                            otherObj->m_bStable = false;
                        // the object is damaged
                        otherObj->damage(obj->m_iDetonateDmg);
                        // check if it's a worm, and if yes, if it dies
                        if (otherObj->type() == WORM && otherObj->m_bAlive == false)
                        {
                            Worm* worm = static_cast<Worm*>(otherObj.get());
                            wormDead(worm);
                        }
                    }
                }
                continue;
            }

            // move
            if (collision.collision == true)
            {
                obj->vx = collision.refl_x * obj->m_fBounceFriction;
                obj->vy = collision.refl_y * obj->m_fBounceFriction;

                if (sqrtf(obj->vx*obj->vx + obj->vy*obj->vy) < obj->m_fMoveThreshold)
                    obj->m_bStable = true;
            }
            // object falls off the map
            else if (newY >= m_iHeight)
            {
                obj->m_bOnScreen = false;
            }
            else if (newX <= obj->m_fRadius)
            {
                obj->x == 0 + obj->m_fRadius * 2;
                obj->vx *= -1.0f * obj->m_fBounceFriction;
                obj->vy *= obj->m_fBounceFriction;
            }
            else if (newX >= m_iWidth - obj->m_fRadius)
            {
                obj->x == m_iWidth - obj->m_fRadius * 2;
                obj->vx *= -1.0f * obj->m_fBounceFriction;
                obj->vy *= obj->m_fBounceFriction;
            }
            else
            {
                obj->x = newX;
                obj->y = newY;
            }
        }
    }

    // remove detonated objects
    logging::log("Removing detonated objects", logging::TRACE);

    m_worldObjects.erase(
            remove_if(
                m_worldObjects.begin(),
                m_worldObjects.end(),
                [](const WObj obj){return obj->m_bDetonated;}),
            m_worldObjects.end());

    // add new objects to the world objects list, e.g. for detonation debris
    for (auto o : newObjects)
        m_worldObjects.push_back(o);

    // calculate TTL and maybe remove objects
    for (auto obj : m_worldObjects)
        if (obj->m_bExpires)
            obj->m_fTtl -= fElapsedTime;
    m_worldObjects.erase(
            remove_if(
                m_worldObjects.begin(),
                m_worldObjects.end(),
                [](WObj obj){return obj->m_bExpires && obj->m_fTtl<=0;}),
            m_worldObjects.end());

    // remove objects that have fallen off the map
    m_worldObjects.erase(
            remove_if(
                m_worldObjects.begin(),
                m_worldObjects.end(),
                [](const WObj obj){return obj->m_bOnScreen==false;}
            ),
            m_worldObjects.end());

    logging::log("Object physics", logging::TRACE);
    for (auto obj : m_worldObjects)
    {
        obj->onObjectUpdate(fElapsedTime);
    }

    // drawing
    logging::log("Updating screen", logging::TRACE);
    accumulateTime += fElapsedTime;
    if (accumulateTime >= wantFPS)
    {
        clear();
        // terrain
        for (int i=0; i<m_iWidth; ++i)
            for (int j=0; j<m_iHeight; ++j)
                if (m_vTerrain[i][j] == T_SOLID)
                    drawPoint(i, j);
        // objects
        for (auto obj : m_worldObjects)
            if (obj->m_bOnScreen)
                obj->draw();

        // game stable ?
        if (m_bStable)
        {
            drawRectangle(1,1,m_iWidth-2,m_iHeight-2,GameEngine::Color(0,255,0));
            drawRectangle(2,2,m_iWidth-3,m_iHeight-3,GameEngine::Color(0,255,0));
        }
        else
        {
            drawRectangle(1,1,m_iWidth-2,m_iHeight-2,GameEngine::Color(255,0,0));
            drawRectangle(2,2,m_iWidth-3,m_iHeight-3,GameEngine::Color(255,0,0));
        }
        string stabi = "Waiting for game to stabilize ...";
        GameEngine::Color stabiColor = m_eState==STATE_STABILIZE
                ? GameEngine::Color(255,55,55)
                : GameEngine::Color(80,80,80);
        drawText(stabi, 20, 20, stabiColor);


        unsigned int yText = m_iHeight - 10;
        unsigned int xTextLeft = 20;
        unsigned int xTextRight = m_iWidth - 160;
        // turn time
        string timer = "turn: "
                + to_string(m_fTurnTime)
                + " / "
                + to_string((int)m_fTurnTimeMax);
        drawText(timer, xTextLeft, yText, GameEngine::Color(55,55,155));
        yText -= 15;
        for (auto team : m_teams)
        {
            unsigned int lenBar = 160; 
            unsigned int healthMax = m_iWormsPerTeam * 100;
            unsigned int teamHealth = 0;
            forAllTeamWorms([&teamHealth](Worm* w)
                    {
                        teamHealth += w->m_iHealth;
                    }, team->id);
            float f = (float)lenBar/(float)healthMax;
            drawRectangle(xTextLeft, yText, 160, 15, team->color, false);
            drawRectangle(xTextLeft, yText, teamHealth*f, 15, team->color, true);
            //  highlight active team
            yText -= 2;
            if (team->id == m_iActiveTeam)
                drawText("active", xTextLeft+5, yText);
            //  - highlight AI teams
            if (team->m_bHuman == false)
                drawText("AI", xTextLeft+lenBar-15, yText);
            yText -= 18;
        }

        // engine perf stats
        string fps = string("fps:") + to_string(m_iFps);
        drawText(fps, xTextRight, m_iHeight-10, GameEngine::Color(255,0,0));
        string pps = string("pps:") + to_string(m_iPps);
        drawText(pps, xTextRight+50, m_iHeight-10, GameEngine::Color(255,0,0));
        string objs = "#active objects:";
        int activeObj = 0;
        for (auto o : m_worldObjects)
            if (o->m_bStable == false)
                ++ activeObj;
        objs += to_string(activeObj);
        objs += "/" + to_string(m_worldObjects.size());
        drawText(objs, xTextRight, m_iHeight-30, GameEngine::Color(255,0,0));

        renderSubmit();

        // reset time until next frame
        accumulateTime = 0.0f;
        ++m_iFpsCounter;
    }
    // update game engine performance metrics
    accumulateTimeInfo += fElapsedTime;
    ++m_iPpsCounter;
    if (accumulateTimeInfo >= wantFPSInfo)
    {
        m_iFps = (int)(m_iFpsCounter / accumulateTimeInfo);
        m_iPps = (int)(m_iPpsCounter / accumulateTimeInfo);
        accumulateTimeInfo = 0.0f;
        m_iFpsCounter = 0;
        m_iPpsCounter = 0;
    }

    // events
    m_vEvents.clear();
    SDL_Event event;
    while(SDL_PollEvent(&event))
    {
        switch(event.type)
        {
            case SDL_QUIT:
                stop();
                break;
            default:
                m_vEvents.push_back(event);
                break;
        }
    }

    // turn management
    gameTurn(fElapsedTime);
}

void Game::gameTurn(float fElapsedTime)
{
    logging::log("Game turn FSM", logging::DEBUG);
    if (m_teams[m_iActiveTeam]->m_bHuman == false)
    {
        aiDecision(fElapsedTime);
    }

    switch(m_eState)
    {
        case STATE_END:
            gameEnd();
            break;
        case STATE_MOVE:
            gameTurnMove(fElapsedTime);
            break;
        case STATE_CHARGE:
            gameTurnCharge(fElapsedTime);
            break;
        case STATE_POST_GAME:
            gamePostGame();
            break;
        case STATE_STABILIZE:
            gameTurnStabilize(fElapsedTime);
            break;
        case STATE_STABILIZE_JUMP:
            gameTurnStabilizeJump(fElapsedTime);
            break;
    }
}

void Game::gameTurnMove(float fElapsedTime)
{
    logging::log("FSM state MOVE", logging::DEBUG);
    // turn timer
    m_fTurnTime += fElapsedTime;
    if (m_fTurnTime >= m_fTurnTimeMax)
    {
        m_eState = STATE_STABILIZE;
        return;
    }

    for (SDL_Event event : m_vEvents)
    {
        switch(event.type)
        {
            case SDL_KEYUP:
                switch (event.key.keysym.sym)
                {
                    case SDLK_UP:
                        wormStopAim();
                        break;
                    case SDLK_DOWN:
                        wormStopAim();
                        break;
                    case SDLK_SPACE:
                        wormJump();
                        m_eState = STATE_STABILIZE_JUMP;
                        break;
                    case SDLK_LEFT:
                        wormStopMoving();
                        break;
                    case SDLK_RIGHT:
                        wormStopMoving();
                        break;
                    case SDLK_n:
                        selectNextWorm();
                        break;
                }
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                    case SDLK_DOWN:
                        wormStartAim(false);
                        break;
                    case SDLK_UP:
                        wormStartAim(true);
                        break;
                    case SDLK_LEFT:
                        wormMoveLeft();
                        break;
                    case SDLK_RIGHT:
                        wormMoveRight();
                        break;
                    case SDLK_x:
                        m_eState = STATE_CHARGE;
                        break;
                }
                break;
        }
    }
}

void Game::gameTurnCharge(float fElapsedTime)
{
    logging::log("FSM state CHARGE", logging::DEBUG);
    Worm* worm = getSelectedWorm();

    // turn timer
    m_fTurnTime += fElapsedTime;
    if (m_fTurnTime >= m_fTurnTimeMax)
    {
        worm->weaponShoot();
        m_eState = STATE_STABILIZE;
        return;
    }

    // weapon charging
    worm->weaponCharge(fElapsedTime);
    if (worm->m_fCharge >= Worm::CHARGE_MAX_VALUE)
    {
        worm->weaponShoot();
        if (m_teams[m_iActiveTeam]->m_bHuman == false)
        {
            m_eStateAI = STATE_AI_STABILIZE;
        }
        m_eState = STATE_STABILIZE;
        return;
    }

    for (SDL_Event event : m_vEvents)
    {
        switch(event.type)
        {
            case SDL_KEYUP:
                switch (event.key.keysym.sym)
                {
                    case SDLK_x:
                        worm->weaponShoot();
                        m_eState = STATE_STABILIZE;
                        break;
                }
                break;
        }
    }
}

void Game::gameTurnStabilize(float fElapsedTime)
{
    logging::log("FSM state STABILIZE", logging::DEBUG);

    if (m_bStable)
    {
        m_eState = STATE_MOVE;
        m_fTurnTime = 0.0f;
        // check game end condition
        if (m_iAliveTeams > 1)
            selectNextTeam();
        else
            m_eState = STATE_END;
    }
}

void Game::gameTurnStabilizeJump(float fElapsedTime)
{
    logging::log("FSM state STABILIZE JUMP", logging::DEBUG);

    m_fTurnTime += fElapsedTime;
    if (m_fTurnTime >= m_fTurnTimeMax)
    {
        m_eState = STATE_STABILIZE;
        return;
    }

    if (m_bStable)
    {
        m_eState = STATE_MOVE;
    }
}

void Game::aiCalcTarget()
{
    if (m_pAiTarget == nullptr)
        return;
    Worm* worm = getSelectedWorm();
    if (worm == nullptr)
        return;

    worm->m_bLeft = worm->x > m_pAiTarget->x;

    int dist = abs(worm->x - m_pAiTarget->x);
    m_fAiAim = 2.5f;
    m_fAiCharge = Worm::CHARGE_MAX_VALUE * 2.0f / 5.0f;
}

void Game::aiDecision(float fElapsedTime)
{
    switch(m_eStateAI)
    {
        case STATE_AI_SELECTWORM:
            selectWormMaxHealth();
            m_eStateAI = STATE_AI_TARGET;
            break;
        case STATE_AI_TARGET:
            {
                auto obj = find_if(
                        m_worldObjects.begin(),
                        m_worldObjects.end(),
                        [this](WObj obj)
                        {
                            return obj->type()==WORM
                                && obj->m_bAlive
                                && obj->team != m_teams[m_iActiveTeam]->id;
                        });
                m_pAiTarget = static_cast<Worm*>((*obj).get());
                aiCalcTarget();
                m_eStateAI = STATE_AI_MOVE;
                break;
            }
        case STATE_AI_MOVE:
            if (aiMoveIntoRange())
                m_eStateAI = STATE_AI_MOVE;
            else
                m_eStateAI = STATE_AI_AIM;
            break;
        case STATE_AI_AIM:
        {
            Worm* worm = getSelectedWorm();
            if (worm->m_bAiming == false)
            {
                wormStartAim(fElapsedTime);
            }
            else if (worm->m_fAim >= m_fAiAim)
            {
                wormStopAim();
                // face the target
                worm->m_bLeft = m_pAiTarget->x < worm->x
                        ? true
                        : false;
                m_eStateAI = STATE_AI_SHOOT;
            }
            break;
        }
        case STATE_AI_SHOOT:
        {
            Worm* worm = getSelectedWorm();
            if (worm->m_fCharge <= m_fAiCharge)
            {
                m_eState = STATE_CHARGE;
            }
            else
            {
                worm->weaponShoot();
                m_eState = STATE_STABILIZE;
                m_eStateAI = STATE_AI_STABILIZE;
                m_pAiTarget = nullptr;
            }
            break;
        }
        case STATE_AI_STABILIZE:
            m_eStateAI = STATE_AI_STABILIZE;
            break;
    }
}

bool Game::aiMoveIntoRange()
{
    if (!m_pAiTarget)
        return false;

    Worm* worm = getSelectedWorm();
    float targetDist = m_pAiTarget->x - worm->x;
    float absDist = abs(targetDist);

    // stop if in range
    if (absDist >= m_fAiTargetDistMin
            && absDist <= m_fAiTargetDistMax)
    {
        if (worm->m_bMoving)
            worm->moveStop();
        return false;
    }

    // targetDist < 0 => target is to the left
    bool moveLeft = true;
    // get away
    if (absDist < m_fAiTargetDistMin)
    {
        if (targetDist <= 0)
            moveLeft = false;
    }
    // get closer
    else
    {
        if (targetDist >= 0)
            moveLeft = false;
    }
    worm->m_bMoving = true;
    worm->m_bLeft = moveLeft;
    return true;
}

/*
 * Main
 */

int main(int argc, char** argv)
{
    Game game;
    game.run();
}
