#include "perlin.h"

#include <iostream>
#include <sstream>
using namespace std;

class Perlin1d : public IPerlin
{
public:
    Perlin1d(unsigned len=16, bool debug=false)
            : uLen(len), bDebug(debug)
    {
        randValues.clear();
        for (int i=0; i<uLen; ++i)
            randValues.push_back((float)(rand()%101 - 50)/50.0f);
    };
    unsigned get_len() {return uLen;}
    virtual float perlin(float x);
    virtual float perlin(float x, int octaves, float persistence, float frequency);
    virtual void perlin_range(int, float, float);
    virtual pair<float, float> get_min_max();
    virtual vector<float> get_n_values(int);
    virtual vector<float> get_values();
private:
    void debug();

    bool bDebug;
    unsigned uLen;
    vector<float> randValues;
    vector<float> values;
    stringstream dbg;
};

void Perlin1d::debug()
{
    if (bDebug)
        cout << dbg.str();
    dbg.str(string());
}

float smooth(float t)
{
    return t * t * t * (t * (t * 6 - 15) + 10);
}

float Perlin1d::perlin(float x)
{
    dbg << "x: " << x << endl;

    float lbound = (int)x;
    float rbound = lbound + 1.0f;
    dbg << "bounds: " << lbound << ", " << rbound << endl;

    float lvalue = randValues[lbound];
    float rvalue = randValues[rbound];
    float ldist = x - lbound;
    float rdist = rbound - x;
    dbg << "vectors: "
        << lvalue << ", "
        << rvalue << ", "
        << ldist << ", "
        << rdist << endl;

    float linfl = lvalue * ldist;
    float rinfl = rvalue * rdist;
    dbg << "influences: " << linfl << ", " << rinfl << endl;

    float weight = x - lbound;
    weight = smooth(weight);
    float p = linfl + weight*(rinfl - linfl);
    dbg << "result: " << p << endl;

    debug();
    return p;
}

float Perlin1d::perlin(float x, int octaves, float persistence, float freq)
{
    float value = 0.0f;
    float amplitude = 1.0f;
    float max = 0.0f;
    for (int i=0; i<octaves; ++i)
    {
        max += amplitude;
        value += perlin(x / freq) * amplitude;
        amplitude *= persistence;
        freq *= 2;
        dbg << x << " -- i: " << i
            << endl << "octave: " << i+1 << " of " << octaves
            << endl << "value: " << value
            << endl << "freq: " << freq
            << endl << "persistence: " << persistence
            << endl << "amplitude: " << amplitude 
            //<< endl << "max: " << max
            //<< endl << "val/max: " << value/max
            << endl
            << endl;
        debug();
    }

    return value;
}

void Perlin1d::perlin_range(int octaves=1, float persistence=2.0f, float frequency=10.0f)
{
    values.clear();
    for (float i=0.0f; i<uLen; ++i)
        values.push_back(perlin(i, octaves, persistence, frequency));
}

pair<float,float> Perlin1d::get_min_max()
{
    pair<float,float> mm = {0.0f, 0.0f};
    for (auto v : values)
    {
        if (v < mm.first)
            mm.first = v;
        if (v > mm.second)
            mm.second = v;
    }
    return mm;
}

vector<float> Perlin1d::get_n_values(int n)
{
    if (n >= values.size())
        return values;

    int step = values.size() / n;
    vector<float> r;
    for (int i=0; i<values.size(); i+=step)
        r.push_back(values[i]);

    return r;
}

vector<float> Perlin1d::get_values()
{
    return values;
}
