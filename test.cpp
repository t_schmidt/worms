#include <algorithm>
#include <chrono>
#include <iostream>
#include <memory>
#include <vector>
using namespace std;

#include <SDL.h>

struct Bla
{
    int blubb = 42;
};

typedef shared_ptr<Bla> Bp;

template<typename T>
void vout(vector<T>& v)
{
    for (auto e : v)
        cout << e << " ";
    cout << endl;
}

void test_vector()
{
    vector<int> v = {3,2,5,87,3,2,6};
    vout(v);
    for (auto i=v.begin(); i!=v.end(); ++i)
    {
        if (*i == 87)
            v.erase(i);
    }
    vout(v);
}

void test_bla(Bp b)
{
    cout << "got bla: " << b->blubb << endl;
}

void test_uptr()
{
    vector<Bp> v;
    //shared_ptr<Bla> b(new Bla());
    Bp b(new Bla());
    v.push_back(move(b));
    vout(v);
    test_bla(v[0]);
}

class X {};
class Y : public X {};
class Z : public X {};
void test_iterator_stuff()
{
    vector<int> v;
    v.push_back(0);
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    v.push_back(4);

    //auto result = find_if(v.begin(), v.end(), [](int x){return x==1;});
    //cout << (result==v.end() ? "end" : "not end") << endl;
    //cout << "res: " << *result << endl;

    vector<int>::iterator two = find_if(v.begin(), v.end(), [](int x){return x==2;});
    cout << "two: " << *two << endl;

    auto search_end = find_if(two, v.end(), [](int x){return x==1;});
    cout << "search_end at end: " << (search_end==v.end() ? "yep" : "nope") << endl;

    auto search_front = find_if(v.begin(), two, [](int x){return x==1;});
    cout << "search_front value: " << *search_front << endl;
}

// draw a circle/half-circle via polar coords method
void test_draw_circle()
{
    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window* window = SDL_CreateWindow(
        "title",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        400,400,
        SDL_WINDOW_SHOWN);


    if (window == nullptr)
    {
        return;
    }

    SDL_Surface* surface = SDL_GetWindowSurface(window);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);

    auto t0 = std::chrono::system_clock::now();
    auto t1 = t0;
    while(true)
    {
        t1 = std::chrono::system_clock::now();
        std::chrono::duration<float> d = t1 - t0;
        if (d.count() >= 4)
            break;

        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        SDL_RenderClear(renderer);

        // circle stuff
        float mx = 200.0f,
            my = 200.0f,
            r = 42.0f;
        float pi = 3.14159f;
        float hpi = pi / 2.0f;
        float ohpi = pi + hpi;
        float twopi = 2*pi;
        for (float i=hpi; i<ohpi; i+=0.01f)
        {
            float x = mx + r * cos(i);
            float y = my + r * sin(i);

            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
            SDL_RenderDrawPoint(renderer, x, y);
        }

        for (float i=(-1.0f)*hpi; i<hpi; i+=0.01f)
        {
            float x = mx + r * cos(i);
            float y = my + r * sin(i);

            SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
            SDL_RenderDrawPoint(renderer, x, y);
        }

        for (float i=0.0f; i<pi; i+=0.4f)
        {
            float x = mx + r * sin(i);
            float y = my + r * cos(i);
            float lx = mx + (-1.0f) * r * sin(i);
            float ly = my + r * cos(i);

            int dim = 4;
            int dimh = dim/2;
            SDL_Rect r = {x-dimh, y-dimh, dim, dim};
            SDL_Rect lr = {lx-dimh, ly-dimh, dim, dim};
            SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
            SDL_RenderFillRect(renderer, &r);
            SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
            SDL_RenderFillRect(renderer, &lr);
        }

        SDL_RenderPresent(renderer);
    }
}

struct Hey
{
    Hey(int _id) : id(_id) {}
    int id;
};
struct Container
{
    Container()
    {
        v.push_back(Hey(3));
    }
    void print_v()
    {
        for (auto e : v)
            cout << e.id << endl;
    }
    vector<Hey> v;
};
void test_store_objlist()
{
    Container c;
    c.print_v();
}

/*
 * Test reflection vector
 */
struct Vector
{
    Vector() {}
    Vector(float x, float y)
            : x1(x), y1(y)
    {}
    Vector(float x, float y, float x1, float y1)
            : x0(x), y0(y), x1(x1), y1(y1)
    {}
    float x0 = 0;
    float y0 = 0;
    float x1 = 0;
    float y1 = 0;
    Vector& operator+(const Vector& other)
    {
        x0 += other.x0;
        y0 += other.y0;
        x1 += other.x1;
        y1 += other.y1;
        return *this;
    }
    void cout()
    {
        std::cout << "(" << x0 << "," << y0 << ")"
            << " -> (" << x1 << "," << y1 << ")"
            << endl;
    }
    float dot(const Vector& other)
    {
       return x1 * other.x1 + y1 * other.y1;
    }
    Vector* refl(const Vector& other)
    {
        Vector *r = new Vector();
        r->x1 = -2.0f * dot(other) * other.x1 + x1;
        r->y1 = -2.0f * dot(other) * other.y1 + y1;
        return r;
    }
};
void test_reflections()
{
    Vector v0 = {1,2,0,0};
    Vector v1 = {-1,2};
    Vector n = {1,1};
    cout << v0.dot(v1) << endl;
    Vector* refl = v0.refl(n);
    refl->cout();
}

/*
 * calculations
 */
void test_calc()
{
    int xm = 430;
    int ym = 210;
    int r = 100;
    // x = xm + r * cos t
    // y = ym + r * sin t

    float pi = 3.14159f;
    float twopi = 2.0f * pi;
    float step = twopi / 4.0f;
    cout << " ----- " << endl;
    for (float t=0.0f; t<=twopi; t+=step)
    {
        cout << "t: " << t << endl;
        float x = xm + r * cos(t);
        float y = ym + r * sin(t);
        cout << "x,y: " << x << ", " << y << endl;
        float tacos = acos((x - xm)/r);
        float tasin = asin((y - ym)/r);
        cout << "tacos: " << tacos << endl;
        cout << "tasin: " << tasin << endl;

        if (y < ym)
        {
            tacos = pi + pi - tacos;
            cout << "tacos now: " << tacos << endl;
        }

        cout << " ----- " << endl;
    }
}

int main(int argc, char** argv)
{
    test_calc();
    return 0;
}
