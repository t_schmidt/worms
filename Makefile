SDLCFG = sdl2-config
WXCFG = 

CXX_FLAGS = -std=c++14 -g -O0
LD_FLAGS = 

WX_CXX_FLAGS = `wx-config --cxxflags`
WX_LD_FLAGS = `wx-config --libs`

SDL_CXXFLAGS = `$(SDLCFG) --cflags`
SDL_LDFLAGS = `$(SDLCFG) --libs` -lSDL2_ttf

PROG = worms perlin test

all: $(PROG)


worms: gameSdl.h worms.cpp tools.h
	g++ -c worms.cpp $(CXX_FLAGS) $(SDL_CXXFLAGS)
	g++ -o worms worms.o $(LD_FLAGS) $(SDL_LDFLAGS)

test: test.cpp
	g++ -o test test.cpp $(CXX_FLAGS) #$(SDL_CXXFLAGS) $(SDL_LDFLAGS)

perlin: perlin.cpp
	g++ -c $< $(CXX_FLAGS) $(SDL_CXXFLAGS)
	g++ -o $@ perlin.o $(LD_FLAGS) $(SDL_LDFLAGS)

.PHONY clean:
	rm *.o $(PROG)

