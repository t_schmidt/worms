#ifndef GAMESDL_H
#define GAMESDL_H

#include <SDL.h>
#include <SDL_ttf.h>

#include <iostream>
using namespace std;

typedef unsigned char uint8;

namespace GameEngine
{
    struct Color
    {
        Color(uint8 r, uint8 g, uint8 b, uint8 a) : r(r), g(g), b(b), a(a) {}
        Color(uint8 r, uint8 g, uint8 b) : r(r), g(g), b(b), a(255) {}
        Color() : r(0), g(0), b(0), a(255) {}
        uint8 r;
        uint8 g;
        uint8 b;
        uint8 a;
    };

    class Engine
    {
        bool onInit();
    public:
        Engine()
        {
            init();
        }
        Engine(int w, int h)
                : m_iWidth(w), m_iHeight(h)
        {
            init();
        }
        ~Engine() 
        {
            SDL_DestroyRenderer(m_renderer);
            SDL_DestroyWindow(m_window);
            TTF_CloseFont(m_font);
            TTF_Quit();
            SDL_Quit();
        }
        void init();
        void run();
        void stop();

        std::chrono::time_point<std::chrono::system_clock> t1, t2;

    protected:
        virtual void onGameUpdate(float) = 0;

        /* drawing */

        void clear()
        {
            SDL_SetRenderDrawColor(m_renderer, m_bgColor.r, m_bgColor.g, m_bgColor.b, m_bgColor.a);
            SDL_RenderClear(m_renderer);
        }

        void drawLine(int x1, int y1, int x2, int y2, const Color& color)
        {
            SDL_SetRenderDrawColor(m_renderer, color.r, color.g, color.b, color.a);
            SDL_RenderDrawLine(m_renderer, x1, m_iHeight-y1, x2, m_iHeight-y2);
        }
        void drawLine(int x1, int y1, int x2, int y2)
        {
            drawLine(x1, y1, x2, y2, m_fgColor);
        }

        void drawPoint(int x, int y, const Color& color)
        {
            SDL_SetRenderDrawColor(m_renderer, color.r, color.g, color.b, color.a);
            SDL_RenderDrawPoint(m_renderer, x, m_iHeight-y);
        }
        void drawPoint(int x, int y)
        {
            drawPoint(x, y, m_fgColor);
        }

        void drawCircle(int x0, int y0, int radius, const Color& color)
        {
            SDL_SetRenderDrawColor(m_renderer, color.r, color.g, color.b, color.a);
            // raster circle
            // https://de.wikipedia.org/wiki/Bresenham-Algorithmus#Kreisvariante_des_Algorithmus
            int f = 1 - radius;
            int ddF_x = 0;
            int ddF_y = -2 * radius;
            int x = 0;
            int y = radius;

            SDL_RenderDrawPoint(m_renderer, x0, m_iHeight-y0 + radius);
            SDL_RenderDrawPoint(m_renderer, x0, m_iHeight-y0 - radius);
            SDL_RenderDrawPoint(m_renderer, x0 + radius, m_iHeight-y0);
            SDL_RenderDrawPoint(m_renderer, x0 - radius, m_iHeight-y0);

            while(x < y)
            {
                if(f >= 0)
                {
                    y--;
                    ddF_y += 2;
                    f += ddF_y;
                }
                x++;
                ddF_x += 2;
                f += ddF_x + 1;

                SDL_RenderDrawPoint(m_renderer, x0 + x, m_iHeight-y0 + y);
                SDL_RenderDrawPoint(m_renderer, x0 - x, m_iHeight-y0 + y);
                SDL_RenderDrawPoint(m_renderer, x0 + x, m_iHeight-y0 - y);
                SDL_RenderDrawPoint(m_renderer, x0 - x, m_iHeight-y0 - y);
                SDL_RenderDrawPoint(m_renderer, x0 + y, m_iHeight-y0 + x);
                SDL_RenderDrawPoint(m_renderer, x0 - y, m_iHeight-y0 + x);
                SDL_RenderDrawPoint(m_renderer, x0 + y, m_iHeight-y0 - x);
                SDL_RenderDrawPoint(m_renderer, x0 - y, m_iHeight-y0 - x);
            }
        }
        void drawCircle(int x0, int y0, int radius)
        {
            drawCircle(x0, y0, radius, m_fgColor);
        }

        void drawRectangle(int x, int y, int w, int h, const Color& color, bool fill)
        {
            SDL_SetRenderDrawColor(m_renderer, color.r, color.g, color.b, color.a);
            SDL_Rect r = {x, m_iHeight-y, w, h};
            if (fill)
            {
                SDL_RenderFillRect(m_renderer, &r);
            }
            else
            {
                SDL_RenderDrawRect(m_renderer, &r);
            }
        }
        void drawRectangle(int x, int y, int w, int h, const Color& c)
        {
            drawRectangle(x, y, w, h, c, false);
        }
        void drawRectangle(int x, int y, int w, int h)
        {
            drawRectangle(x, y, w, h, m_fgColor, false);
        }

        void drawText(const std::string& t, int x, int y, const Color& color)
        {
            SDL_Color fontColor =
            {
                color.r,
                color.g,
                color.b
            };
            SDL_Surface* txtsurf = TTF_RenderText_Solid(m_font, t.data(), fontColor);
            SDL_Texture* tt = SDL_CreateTextureFromSurface(m_renderer, txtsurf);
            int tw = 0;
            int th = 0;
            SDL_QueryTexture(tt, nullptr, nullptr, &tw, &th);
            SDL_Rect r = {x,m_iHeight-y,tw,th};
            SDL_RenderCopy(m_renderer, tt, nullptr, &r);
        }
        void drawText(const std::string& t, int x, int y)
        {
            drawText(t, x, y, m_fgColor);
        }

        void renderSubmit()
        {
            SDL_RenderPresent(m_renderer);
        }

        float getTotalTime()
        {
            return totalTime;
        }

        /* controls */

        // TODO
        void keyPressed(int) {}
        void OnLeftClick(int, int) {}
        void OnRightClick(int, int) {}

    private:
        SDL_Surface* m_surface;
        SDL_Window* m_window;
        SDL_Renderer* m_renderer;
        TTF_Font* m_font;
        float m_fRunMaxSeconds = 5.0f;
        bool m_bGameRunning;
        float totalTime = 0.0f;
        Color m_fgColor;
        Color m_bgColor = {255, 255, 255, 255};
        int m_iHeight = 400;
        int m_iWidth = 400;
    };

    void Engine::init()
    {
        SDL_Init(SDL_INIT_VIDEO);

        m_window = SDL_CreateWindow(
                "SDL test",
                SDL_WINDOWPOS_UNDEFINED,
                SDL_WINDOWPOS_UNDEFINED,
                m_iWidth, m_iHeight,
                SDL_WINDOW_SHOWN);

        if (m_window != nullptr)
        {
            m_surface = SDL_GetWindowSurface(m_window);
            m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_SOFTWARE);
        }

        TTF_Init();
        m_font = TTF_OpenFont("consola.ttf", 12);
    }

    void Engine::run()
    {
        auto t0 = std::chrono::system_clock::now();
        auto t1 = t0;
        auto t2 = t0;

        int wantFPS = 60;
        float waitTime = 1.0f / (float)wantFPS;
        float waited = 0.0f;

        m_bGameRunning = true;
        while (m_bGameRunning)
        {
            t2 = std::chrono::system_clock::now();
            std::chrono::duration<float> elapsedTime = t2 - t1;
            float fElapsedTime = elapsedTime.count();
            totalTime += elapsedTime.count();
            t1 = t2;

            onGameUpdate(fElapsedTime);
        }
    }

    void Engine::stop()
    {
        m_bGameRunning = false;
    }
};

#endif /* !GAMESDL_H */
