#ifndef PERLIN_H
#define PERLIN_H

#include <vector>

class IPerlin
{
public:
    virtual float perlin(float) = 0;
    virtual float perlin(float, int, float, float) = 0;
    virtual void perlin_range(int, float, float) = 0;
    virtual std::pair<float, float> get_min_max() = 0;
    virtual std::vector<float> get_n_values(int) = 0;
    virtual std::vector<float> get_values() = 0;
};

#endif
