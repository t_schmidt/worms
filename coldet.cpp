/*
 * compile with:

SDL_CXXFLAGS = `$(SDLCFG) --cflags`
SDL_LDFLAGS = `$(SDLCFG) --libs`

coldet: coldet.cpp
	g++ -c coldet.cpp $(CXX_FLAGS) $(SDL_CXXFLAGS)
	g++ -o coldet coldet.o $(LD_FLAGS) $(SDL_LDFLAGS)

*/

#include <chrono>
#include <iostream>
#include <thread>
#include <vector>

using namespace std;

#include <SDL.h>
#include <SDL_ttf.h>

typedef unsigned char uint8;

struct Coldet;

struct Color
{
    Color(uint8 r, uint8 g, uint8 b, uint8 a) : r(r), g(g), b(b), a(a) {}
    Color(uint8 r, uint8 g, uint8 b) : r(r), g(g), b(b), a(255) {}
    Color() : r(0), g(0), b(0), a(255) {}
    uint8 r;
    uint8 g;
    uint8 b;
    uint8 a;
};

struct Obj
{
    Obj(Coldet* coldet) : cd(coldet) {}
    Obj(int x, int y, int r, int vx, int vy, Coldet* coldet)
            : x(x), y(y), r(r), vx(vx), vy(vy), cd(coldet)
    {}
    int x;
    int y;
    int r;
    int vx;
    int vy;
    Color c = {255,0,0,255};
    void draw();
    Coldet* cd;
    bool out = true;
};

struct Coldet
{
    Coldet()
    {
        status = init();
    }
    SDL_Window* window;
    SDL_Surface* surface;
    SDL_Renderer* renderer;
    TTF_Font* font;
    int status = 0;
    int h = 600;
    int w = 600;
    Color m_fgColor;
    Color m_bgColor = {200,200,200,255};
    vector<vector<bool>> terrain;
    vector<Obj> objects;
    bool m_bDraw = true;

    int init();

    void clear();
    void drawCircle(int,int,int,const Color&);
    void drawLine(int,int,int,int,const Color&);
    void drawPoint(int,int);
    void drawPoint(int,int,const Color&);
    void go();
};

int Coldet::init()
{
    SDL_Init(SDL_INIT_VIDEO);
    window = SDL_CreateWindow(
        "coldet",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        w,h,
        SDL_WINDOW_SHOWN
    );

    if (window == nullptr)
        return 1;

    surface = SDL_GetWindowSurface(window);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);

    TTF_Init();
    font = TTF_OpenFont("consola.ttf", 12);

    // terrain
    for (int i=0; i<w; ++i)
    {
        vector<bool> v;
        for (int j=0; j<h; ++j)
            v.push_back(false);
        terrain.push_back(v);
    }
    int min = (-1) * w/2;
    int max = w/2;
    for (int x=min; x<max; ++x)
    {
        int y = (-0.0015) * x*x + h - 400;
        for (int i=0; i<y; ++i)
            terrain[x+w/2][i] = true;
    }

    // objects
    Obj obj(430,210,100,-80,0,this);
    objects.push_back(obj);

    return 0;
}

void Coldet::drawCircle(int x0, int y0, int radius, const Color& color)
{
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    // raster circle
    // https://de.wikipedia.org/wiki/Bresenham-Algorithmus#Kreisvariante_des_Algorithmus
    int f = 1 - radius;
    int ddF_x = 0;
    int ddF_y = -2 * radius;
    int x = 0;
    int y = radius;

    SDL_RenderDrawPoint(renderer, x0, h-y0 + radius);
    SDL_RenderDrawPoint(renderer, x0, h-y0 - radius);
    SDL_RenderDrawPoint(renderer, x0 + radius, h-y0);
    SDL_RenderDrawPoint(renderer, x0 - radius, h-y0);

    while(x < y)
    {
        if(f >= 0)
        {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x + 1;

        SDL_RenderDrawPoint(renderer, x0 + x, h - y0 + y);
        SDL_RenderDrawPoint(renderer, x0 - x, h - y0 + y);
        SDL_RenderDrawPoint(renderer, x0 + x, h - y0 - y);
        SDL_RenderDrawPoint(renderer, x0 - x, h - y0 - y);
        SDL_RenderDrawPoint(renderer, x0 + y, h - y0 + x);
        SDL_RenderDrawPoint(renderer, x0 - y, h - y0 + x);
        SDL_RenderDrawPoint(renderer, x0 + y, h - y0 - x);
        SDL_RenderDrawPoint(renderer, x0 - y, h - y0 - x);
    }
}
void Coldet::drawLine(int x1, int y1, int x2, int y2, const Color& color)
{
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    SDL_RenderDrawLine(renderer, x1, h-y1, x2, h-y2);
}
void Coldet::drawPoint(int x, int y, const Color& color)
{
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    SDL_RenderDrawPoint(renderer, x, h-y);
}
void Coldet::drawPoint(int x, int y)
{
    drawPoint(x, y, m_fgColor);
}

void Coldet::clear()
{
    SDL_SetRenderDrawColor(renderer, m_bgColor.r, m_bgColor.g, m_bgColor.b, m_bgColor.a);
    SDL_RenderClear(renderer);
}
void Coldet::go()
{
    while (1)
    {
        // events
        SDL_Event eve;
        while (SDL_PollEvent(&eve))
        {
            if (eve.type == SDL_QUIT)
                exit(0);
        }

        // drawing
        if (m_bDraw)
        {
            clear();
            for (int i=0; i<w; ++i)
                for (int j=0; j<h; ++j)
                    if (terrain[i][j])
                        drawPoint(i,j);
            for (Obj obj : objects)
            {
                obj.draw();
            }
            // no continous updates
            m_bDraw = false;
        }
        SDL_RenderPresent(renderer);
        this_thread::sleep_for(chrono::milliseconds(400));
    }
}

void Obj::draw()
{
    cd->drawCircle(x,y,r,c);
    cd->drawCircle(x,y,2,c);


    // previous obj position
    float x0 = x - vx;
    float y0 = y - vy;
    cd->drawCircle(x0, y0, 3, Color(0,0,0));

    // direction of movement
    float lenFact = r / sqrtf(vx*vx + vy*vy);
    float xc = x + vx * lenFact;
    float yc = y + vy * lenFact;

    float te = acos((xc - x) / r);
    float pi = 3.14519f;
    float pihalf = 3.14519f / 2.0f;
    float step = 3.14159f / 16.0f;
    float respx =  0.0f;
    float respy =  0.0f;
    // correct the parameter for the lower semi-circle
    if (yc < y)
        te = pi + pi - te;

    // min dist to terrain (in movement direction)
    // will be used for the y adjustment afterwards
    float distMin = 600.0f; // max of height and width
    float txMin = 0.0f;
    float tyMin = 0.0f;

    for (float p=te-pihalf; p<=te+pihalf; p+=step)
    {
        float px = x + r * cos(p);
        float py = y + r * sin(p);
        cd->drawLine(x, y, px, py, Color(70,70,200));
        if (cd->terrain[(int)px][(int)py])
        {
            cd->drawCircle(px,py,2,Color(255,255,70));
            // test: OLC response vector
            respx += x - px ;
            respy += y - py ;

            // check the line between x,y and px,py for the first point which is solid terrain
            cout << "get distance to terrain: " << px << ", " << py << endl;
            float ycorrStep = r / 8.0f;
            for (float d=0.0f; d<=r; d+=ycorrStep)
            {
                cout << "d: " << d << endl;
                float tx = x + d * cos(p);
                float ty = y + d * sin(p);
                cout << "txy: " << tx << ", " << ty << endl;
                if (cd->terrain[tx][ty])
                {
                    if (d < distMin)
                    {
                        distMin = d;
                        txMin = tx;
                        tyMin = ty;
                    }
                    cd->drawCircle(tx, ty, 2, Color(0,255,0));
                }
            }
        }
    }
    cd->drawLine(x0, y0, x, y, Color(0,255,0));

    cout << "distMin: " << distMin << endl;
    cout << "txyMin: " << txMin << ", " << tyMin << endl;
    cd->drawCircle(txMin, tyMin, 3, Color(255,0,0));

    // length of the calculated response vector
    float respLen = sqrtf(respx*respx + respy*respy);

    // the surface normal is treated as a drectional vector
    // the reflection vector is drawn with 200px length
    float respx50 = respx/(respLen/200.0f);
    float respy50 = respy/(respLen/200.0f);
    cd->drawLine(x, y, x+respx50, y+respy50, Color(0,255,255));

    // direction of movement has to be drawn after the rest, otherwise it's covered up
    cd->drawLine(x, y, xc, yc, Color(255,200,0));


    // shortened response vector of length 1(?)
    float respsx = respx/respLen;
    float respsy = respy/respLen;
    cd->drawLine(x,y,x+respsx,y+respsy,Color(0,0,0));

    // r = -2 * d * n + v
    float dotn = vx * respsx + vy * respsy;
    float rx = -2.0f * dotn * respsx + vx;
    float ry = -2.0f * dotn * respsy + vy;
    // finally, the reflected vector
    cd->drawLine(x,y,x+rx,y+ry,Color(200,255,50));

    // looks good
    cd->drawCircle(x, r+tyMin, r, Color(80,80,80));

    // TODO
    // the parameters for the necessary height adjustment should be returned
    // there should be a maximum for this adjustment which should be integrated in the check above
    // the height adjustment check above should be made optional by function parameter
    // the caller should then do another check for collisions in the upward height correction direction
    //  if any collisions occur there, there should be another adjustment in the right direction (backwards)
    //  (this might trigger another collision -.-)
    // this procedure must be adapted for movement to the right
}

int main(int argc, char** argv)
{
    Coldet c;
    c.go();
}
