#include <wx/wxprec.h>
#ifndef WXPRECOMP
	#include <wx/wx.h>
#endif
#include <chrono>



/*************************************************************
 * game base class
 ************************************************************/

class GameWindow;

class GameEngine : public wxApp
{
    friend GameWindow;

    bool OnInit();
    void OnIdle(wxIdleEvent& evt);

    GameWindow* gameWindow;
    wxDC* dc;
    std::chrono::time_point<std::chrono::system_clock> t1, t2;

protected:
    virtual void OnGameUpdate(float fElapsedTime) = 0;
    virtual void keyPressed(int) {}
    virtual void OnLeftClick(int x, int y) {}
    virtual void OnRightClick(int x, int y) {}
    void Clear()
    {
        dc->Clear();
    }
    void DrawCircle(int x, int y, int r)
    {
        dc->DrawCircle(x, y, r);
    }
    void DrawLine(int x1, int y1, int x2, int y2)
    {
        dc->DrawLine(x1, y1, x2, y2);
    }
    void DrawPoint(int x, int y)
    {
        dc->DrawPoint(x, y);
    }
    void DrawRectangle(int x, int y, int w, int h)
    {
        dc->DrawRectangle(x, y, w, h);
    }
    void DrawText(const std::string& t, int x, int y)
    {
        dc->DrawText(t, x, y);
    }
    void SetBackground()
    {
       dc->SetBackground(*wxWHITE_BRUSH);
    }
    bool PollKey(char key)
    {
        return wxGetKeyState(wxKeyCode(key));
    }
    bool PollKeyAlt()
    {
        return wxGetKeyState(WXK_ALT);
    }
    bool PollKeyCtrl()
    {
        return wxGetKeyState(WXK_CONTROL);
    }
    bool PollKeyShift()
    {
        return wxGetKeyState(WXK_SHIFT);
    }
};


/*************************************************************
 * game display
 ************************************************************/

class GameWindow : public wxFrame
{
    wxPanel* panel;
    GameEngine* m_Engine;
public:
    GameWindow(GameEngine* app) : wxFrame((wxFrame *)NULL, -1,  wxT("Game?"), wxPoint(50,50), wxSize(400,400)), m_Engine(app)
    {
        panel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxWANTS_CHARS);
        panel->Bind(wxEVT_CHAR, &GameWindow::OnKeyPress, this);
        panel->Bind(wxEVT_LEFT_DOWN, &GameWindow::OnLeftClick, this);
        panel->Bind(wxEVT_RIGHT_DOWN, &GameWindow::OnRightClick, this);
    }
    void OnKeyPress(wxKeyEvent& evt)
    {
        m_Engine->keyPressed(evt.GetKeyCode());
    }
    void OnLeftClick(wxMouseEvent& evt)
    {
        m_Engine->OnLeftClick(evt.GetX(), evt.GetY());
    }
    void OnRightClick(wxMouseEvent& evt)
    {
        m_Engine->OnRightClick(evt.GetX(), evt.GetY());
    }
    wxDC* getDC()
    {
        return new wxClientDC(panel);
    }
};


/*************************************************************
 * impl class GameEngine
 ************************************************************/

bool GameEngine::OnInit()
{
    t1 = std::chrono::system_clock::now();

    Bind(wxEVT_IDLE, &GameEngine::OnIdle, this, wxID_ANY);

    gameWindow = new GameWindow(this);
    gameWindow->Show();
    SetTopWindow(gameWindow);

    return true;
}
void GameEngine::OnIdle(wxIdleEvent& evt)
{
    std::chrono::time_point<std::chrono::system_clock> t2 = std::chrono::system_clock::now();
    std::chrono::duration<float> elapsedTime = t2 - t1;
    float fElapsedTime = elapsedTime.count();
    t1 = t2;

    dc = gameWindow->getDC();
    OnGameUpdate(fElapsedTime);
    delete dc;

    evt.RequestMore();
}
