#include <SDL.h>

#include <iostream>
#include <cmath>
#include <chrono>
#include <thread>
#include <vector>

using namespace std;

struct Perlin
{
    Perlin(int len=16) : iLen(len)
    {
        init_rand1d();
		srand(uSeed);
    }
    void init_rand1d()
    {
        for (int i=0; i<iLen; ++i)
            rand1d.push_back((float)(rand()%101 - 50)/50.0f);
    }
    float doPerlin1d(float);
    float doPerlin1d(float, int, float);
    void doPerlin1dRange();
    int iLen;
	unsigned int uSeed = 1234;
    vector<float> perlin1d;
    vector<float> rand1d;
};

template <typename T>
void vout(vector<T>& v, bool print_ix=false)
{
    int cc = 0;
    for (auto e : v)
    {
        if (print_ix)
            cout << cc++ << " ";
        cout << e << endl;
    }
}

float Perlin::doPerlin1d(float x)
{
    //cout << "x: " << x << endl;

    float lbound = (int)x;
    float rbound = lbound + 1.0f;
    //cout << "bounds: " << lbound << ", " << rbound << endl;

    float lvalue = rand1d[lbound];
    float rvalue = rand1d[rbound];
    float ldist = x - lbound;
    float rdist = x - rbound;
    //cout << "vectors: "
    //    << lvalue << ", "
    //    << rvalue << ", "
    //    << ldist << ", "
    //    << rdist << endl;

    float linfl = lvalue * ldist;
    float rinfl = rvalue * rdist;
    //cout << "influences: " << linfl << ", " << rinfl << endl;

    float p = linfl + ldist * (rinfl - linfl);
    //cout << "result: " << p << endl;
    return p;
}

float Perlin::doPerlin1d(float x, int octaves, float persistence)
{
    float value = 0.0f;
    float freq = 1.0f;
    float amplitude = 1.0f;
    float max = 0.0f;
    for (int i=0; i<octaves; ++i)
    {
        max += amplitude;
        value += doPerlin1d(x * freq) * amplitude;
        amplitude *= persistence;
        freq *= 2;
        if (true)
        {
            cout << x << " -- i: " << i
                << endl << "value: " << value
                << endl << "freq: " << freq
                << endl << "amplitude: " << amplitude 
                << endl << "max: " << max
                << endl << "val/max: " << value/max
                << endl
                << endl;
        }
    }

    return value;
    /*
      double total = 0;
    double frequency = 1;
    double amplitude = 1;
    double maxValue = 0;  // Used for normalizing result to 0.0 - 1.0
    for(int i=0;i<octaves;i++) {
        total += perlin(x * frequency, y * frequency, z * frequency) * amplitude;
        
        maxValue += amplitude;
        
        amplitude *= persistence;
        frequency *= 2;
    }
    
    return total/maxValue;
    */
}

void Perlin::doPerlin1dRange()
{
	perlin1d.clear();
	for (float i=0.0f; i<iLen; ++i)
		perlin1d.push_back(doPerlin1d(i/10.0f, 8, 4));
}

int simple_test()
{
    Perlin perlin;
    vout(perlin.rand1d, true);
    return 0;
}

int sdl_test()
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		cerr << "Failed to init SDL, quit" << endl;
		return 1;
	}
	SDL_Window* window = SDL_CreateWindow(
			"perlin noise?",
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			512, 512,
			SDL_WINDOW_SHOWN);

	if (window == nullptr)
	{
		cerr << "Failed to create window, quit" << endl;
		return 2;
	}
    
    Perlin perlin(512);
    perlin.doPerlin1dRange();
    vector<float> pp = perlin.perlin1d;

	SDL_Surface* surface = SDL_GetWindowSurface(window);
	SDL_FillRect(
			surface, nullptr, 
			SDL_MapRGB(surface->format, 0xff, 0xff, 0xff));

    cout << "pp.size: " << pp.size() << endl;
    for (int i=0; i<pp.size()-1; ++i)
    {
        SDL_Rect r = {i, 128+pp[i], 1, 1};
        SDL_FillRect(surface, &r, SDL_MapRGB(surface->format, 0,0,0));
    }
	SDL_UpdateWindowSurface(window);

    float min=0.0f,max=0.0f;
    for (auto v : perlin.perlin1d)
    {
        if (min > v)
            min = v;
        else if (max < v)
            max = v;
    }
    cout << "min: " << min << ", max: " << max << endl;

    this_thread::sleep_for(chrono::seconds(6));
    return 0;
}

int main(int argc, char** argv)
{
    return sdl_test();
}
